<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Produto;

class Gerenciar extends Controller
{
    public function produtos(){
        $produtos = Produto::all();

        return view('admin.produtos')->with('produtos',$produtos);
    }
}
