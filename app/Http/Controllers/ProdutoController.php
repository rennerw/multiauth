<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Produto;

class ProdutoController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getAll()
    {
        $produtos = Produto::all();
        return $produtos;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        /* $validator = $request->validate([
            'nome' => 'required|max:255',
            'marcar' => 'max:255',
            'preco' => 'required|numeric',
        ]);

        if ($validator->fails()) {
            return response(['status' => 'warning', 'msg' => 'Nome e Preço do produto é obrigatório']);
        } */

        $produto = new Produto();
        $produto->nome = $request->input('nome');
        $produto->preco = $request->input('preco');
        $produto->marca = $request->input('marca');
        $atalho1 = '';
        if ($request->hasFile('img_produto')){
            $arquivo = $request->file('img_produto');

            $nomearquivo1 = "produto-".$produto->nome."-".$produto->marca.".".$arquivo->guessClientExtension();

            $atalho1 = $arquivo->storeAs('redacoes', $nomearquivo1, 'public');

        }
        $produto->url = $atalho1;
        $produto->save();
        return response(['status' => 'success', 'msg' => 'Produto adicionado com sucesso']);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $produto = Produto::find($id);
        return $produto;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {   
        Produto::find($id)->update($request->all());
        return response(['status' => 'success', 'msg' => 'Produto adicionado com sucesso']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Produto::find($id)->delete();
        return response(['status' => 'success', 'msg' => 'Produto adicionado com sucesso']);
    }
}
