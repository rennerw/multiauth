function produtoAction(method){
    event.preventDefault();
    if(method == "POST"){
        $("#form-produto").children().remove();
        $("#form-produto").append(padraoPOST());
        
    }
    else if(method == "UPDATE"){
        url = $(event.target).attr('href');
        $("#form-produto").attr("action",url);
        $("#form-produto").children().remove();
        mostrarProduto(url);
    }
    else if(method == "SHOW"){
        url = $(event.target).attr('href');
        $("#form-produto").attr("action",url);
        $("#form-produto").children().remove();
        console.log($(event.target).attr('href'));
        mostrarProduto(url);
        
        
    }
}

function salvarProduto(){
    var formdata = new FormData($("#form-produto")[0]);
    url = $("#form-produto").attr("action");
    $.ajax({
        url: url,
        data: formdata,
        processData: false,
        contentType: false,
        method: "POST",
	    success: function(data) {
             limpaForm();
             limpaImagem();
             mensagem(data.status,data.msg);
        },
        error: function(data) {

        }
    });
}

function excluirProduto(objeto){
    event.preventDefault();
    url = $(objeto).attr("href");

    $.ajax({
        url: url,
        method: "GET",
	    success: function(data) {
            //console.log(data);
             $(objeto).parent().parent().fadeOut();
        },
        error: function(data) {

        }
    });
}

function mostrarProduto(url){
    $.ajax({
        url: url,
        processData: false,
        contentType: false,
        method: "GET",
	    success: function(data) {
             limpaForm();
             limpaImagem();
             $("#form-produto").append(padraoUPDATE(data));
        },
        error: function(data) {

        },
        
    });
    
}

function padraoPOST(){
    return `<div class="form-group">
    <div id="form-mensagens"></div>
    <label class="col-form-label">Produto:</label>
    <input type="text" class="form-control" name="nome" value="">

    <label class="col-form-label">Marca:</label>
    <input type="text" class="form-control" name="marca" value="">

    <label class="col-form-label">Preço:</label>
    <input type="text" class="form-control" name="preco" value="">

    <label class="col-form-label">Imagem:</label>
    <input type="file" class="form-control" name="img_produto" value="">
    <div id="img-produto"></div>  
    <br>
    <button type="button" onclick="salvarProduto()" class="btn btn-success" data-action="" data-method="">Enviar</button>
  </div>`;
}

function padraoUPDATE(data){
    return `<div class="form-group">
    <div id="form-mensagens"></div>
    <label class="col-form-label">Produto:</label>
    <input type="text" class="form-control" name="nome" value="${data.nome}">

    <label class="col-form-label">Marca:</label>
    <input type="text" class="form-control" name="marca" value="${data.marca}">

    <label class="col-form-label">Preço:</label>
    <input type="text" class="form-control" name="preco" value="${data.preco}">

    <label class="col-form-label">Imagem:</label>
    <input type="file" class="form-control" name="img_produto" value="">
    <div id="img-produto">
    <img src="http://localhost:8000${"/storage/"+data.url}" width='400px' height='300px' alt='Imagem do produto'>
    </div>  
    <br>
    <button type="button" onclick="salvarProduto()" class="btn btn-success">Enviar</button>
    </div>`;
}


function atualizaLista(){
    $.ajax({
        url: '/produto',
        method: "GET",
	    success: function(data) {
            console.log(data);
        },
        error: function(data) {

        }
    });
}

// utilidades
$("input[type='file']").change(function(e){
    if($("#img-produto img")){
        limpaImagem();
    }
    if (this.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#img-produto').append("<img src='' width='400px' height='300px' alt='Imagem do produto'>");
            $("#img-produto img").attr('src',e.target.result)
        }
        reader.readAsDataURL(this.files[0]);
    }

});

function limpaForm(){
    $(':input','form').not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
}

function limpaImagem(){
    $("#img-produto").children("img").remove();
}


/**
 * 
 * @param {Tipo de cor padrão do bootstrap(success, warning, danger... )} color 
 * @param {Mensagem} message 
 * https://getbootstrap.com/docs/4.2/utilities/colors/
 */
function mensagem(color,message){
    var padrao = `<div class="alert alert-${color} alert-dismissible fade show" role="alert">
    <strong> ${message} </strong>
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    </div>`;
    $("#form-mensagens").append(padrao);
}