<form method='POST' id='form-produto' action='{{route('produto.update',$produto->id)}}' aria-label='Cadastrar produto' enctype='multipart/form-data'> 
    <input type='hidden' name='_token' value='{{ csrf_token() }}'>
    <div class="form-group">
            <div id="form-mensagens"></div>
            <label class="col-form-label">Produto:</label>
    <input type="text" class="form-control" name="nome" value="{{$produto->nome}}">
        
            <label class="col-form-label">Marca:</label>
    <input type="text" class="form-control" name="marca" value="{{$produto->marca}}">
        
            <label class="col-form-label">Preço:</label>
            <input type="text" class="form-control" name="preco" value="{{$produto->preco}}">
        
            <button type="submit" class="btn btn-success">Enviar</button>
          </div>`;
</form>