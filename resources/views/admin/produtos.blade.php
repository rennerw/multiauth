@extends('layouts.app')

@section('content')



<div class="container">
    <div class="row justify-content-center">
    <div class="col-md-8">
        <button type="button" onclick="produtoAction('POST')" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
            Adicionar Produto
        </button>
        </div>
    </div>
    <br>
    <div class="row">
    <div class="col-md-12">
            <table class="table table-dark">
                    <thead>
                      <tr>
                        <th scope="col">#</th>
                        <th scope="col">Nome</th>
                        <th scope="col">Produto</th>
                        <th scope="col">Preço</th>
                        <th scope="col">Imagem</th>
                      </tr>
                    </thead>
                    <tbody>
                    @foreach($produtos as $p)
                        <tr class="">
                            <td>{{ $p->id }}</td>
                            <td class=""><a href="{{route("produto.show",$p->id)}}" onclick="produtoAction('SHOW')">{{ ($p->nome != null)? $p->nome : '' }}</a></td>
                            <td class="">{{ ($p->marca != null)? $p->marca : '' }}</td>
                            <td class="">{{ ($p->preco != null)? $p->preco : '' }}</td>
                            <td class="">@if($p->url != null)<a href="{{Storage::url($p->url)}}" target="_blank"><img src="{{Storage::url($p->url)}}" style="width: 150px; height: 150px;"></a>@endif</td>
                            <td><a href="{{route('produto.destroy',$p->id)}}" onclick="excluirProduto(this)">Remover</a></td>
                            <td><a href="{{route('produto.update',$p->id)}}" onclick="produtoAction('UPDATE')" data-toggle="modal" data-target="#exampleModal">Editar</a></td>
                        </tr>
                    @endforeach
                  </table>
    </div>
    </div>
</div>

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Novo Produto</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
        <form method="POST" id="form-produto" action="{{route('produto.store')}}" aria-label="{{ __('Cadastrar Produto') }}" enctype="multipart/form-data">
            @csrf
            
      </div>
    </div>
  </div>
</div>
@endsection
@section('script')
<script type="text/javascript" src="{{ asset('/js/produto.js') }}"></script>
    

@endsection