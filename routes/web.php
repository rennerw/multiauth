<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::view('/', 'welcome');
Auth::routes();

Route::get('/login/admin', 'Auth\LoginController@showAdminLoginForm');
//Route::get('/register/admin', 'Auth\RegisterController@showAdminRegisterForm');

Route::post('/login/admin', 'Auth\LoginController@adminLogin');
//Route::post('/register/admin', 'Auth\RegisterController@createAdmin');

Route::view('/home', 'home')->middleware('auth');
Route::get('/produto',"ProdutoController@getAll");
// logado como admin
Route::group(['middleware' => 'auth:admin'], function()
{
    Route::view('/admin', 'admin.home');
    Route::get('/admin/produto', 'Admin\Gerenciar@produtos')->name('admin.produtos');
    Route::post('/produto','ProdutoController@store')->name('produto.store');
    Route::get('/produto/{id}',"ProdutoController@show")->name('produto.show');
    Route::post('/produto/{id}',"ProdutoController@update")->name('produto.update');
    Route::get('/produto/remove/{id}',"ProdutoController@destroy")->name('produto.destroy');
});
//Route::view('/writer', 'writer');